function Car(motor, carColor, maxSpeed, price){
    this.motor = motor
    this.carColor = carColor
    this.maxSpeed = maxSpeed
    this.price = price
}

const car1 = new Car('v8', 'blue', 320, 32000)
console.log(car1.store)
console.log(car1)



//create a property store that can be accessed at top level chain of object by any other objects
//following above example create whatever function you want and try to access property store from a new instance of it

function House(size, houseColor, price){
    this.size = size
    this.houseColor = houseColor
    this.price = price
}

const house1 = new House(240, 'Cinza', 250000)
house1.__proto__.store = 'teste'
console.log(house1.store)


function Shopping(fastFood){
    this.fastFood = fastFood
}

const shopping1 = new Shopping('McDonalds')
console.log(shopping1.store)
Object.prototype.store = 'Qualquer Objeto'
console.log(Object.prototype)

console.log(shopping1.store)

const buyCar = () => {
    let price = 12000
    if(price <10000){
        console.log(price)
    }else{
        let cpf = true
        var creditCard = true
        if(creditCard){
            let newPrice = 5000
        }
    }
    console.log(creditCard)
    // console.log(cpf)
    console.log(price)
}

//please write expected result for each console.log above

buyCar()